import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { CustomStepperComponent } from './custom-stepper/custom-stepper.component';
import { CustomStepComponent } from './custom-step/custom-step.component';
import { LayerWrapperComponent } from './layer-wrapper/layer-wrapper.component';
import { Routes, RouterModule } from '@angular/router';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { CdkStepperModule } from '@angular/cdk/stepper';
import 'hammerjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ToggleButtonComponent } from './toggle-button/toggle-button.component';
import { MatCardModule } from '@angular/material/card';
import { ButtonGridComponent } from './button-grid/button-grid.component';
import { CustomFormGuard } from './custom-form.guard';
import { NumberPickerDialogComponent } from './number-picker-dialog/number-picker-dialog.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { ResultMessageComponent } from './result-message/result-message.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientModule } from '@angular/common/http';
import { ConfigResolver } from './config.resolve';

const routes: Routes = [
  {
    path: 'app',
    component: LayerWrapperComponent,
    resolve: {
      config: ConfigResolver,
    },
    children: [
      {
        path: 'language',
        component: CustomStepComponent,
        canActivate: [CustomFormGuard],
      },
      {
        path: 'condition',
        component: CustomStepComponent,
        canActivate: [CustomFormGuard],
      },
      {
        path: 'symptoms',
        component: CustomStepComponent,
        canActivate: [CustomFormGuard],
      },
      {
        path: 'information',
        component: CustomStepComponent,
        canActivate: [CustomFormGuard],
      },
    ],
  },
  { path: '**', redirectTo: 'app', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    CustomStepperComponent,
    CustomStepComponent,
    LayerWrapperComponent,
    ToggleButtonComponent,
    ButtonGridComponent,
    NumberPickerDialogComponent,
    ResultMessageComponent,
  ],
  imports: [
    MatDialogModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    RouterModule.forRoot(routes),
    CdkStepperModule,
    NgxHmCarouselModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
