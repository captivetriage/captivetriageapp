import {
  Component,
  OnDestroy,
  Input,
  AfterViewInit,
  forwardRef,
} from '@angular/core';
import { from, of, Subject } from 'rxjs';
import { concatMap, delay, takeUntil } from 'rxjs/operators';
import { trigger, transition, useAnimation } from '@angular/animations';
import { bounceIn } from 'ng-animate';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { toPairs, fromPairs } from 'lodash';

export type ButtonGridType = 'multiple' | 'single';

@Component({
  selector: 'app-button-grid',
  templateUrl: './button-grid.component.html',
  styleUrls: ['./button-grid.component.scss'],
  animations: [
    trigger('buttonpop', [transition('false => true', useAnimation(bounceIn))]),
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ButtonGridComponent),
      multi: true,
    },
  ],
})
export class ButtonGridComponent
  implements OnDestroy, AfterViewInit, ControlValueAccessor {
  @Input() mode: ButtonGridType = 'multiple';
  @Input() buttonConfig;
  buttonAnimations;
  buttons;
  unsubscribeAll = new Subject();
  set value(value: any) {
    if (!this.buttonAnimations) {
      this.buttonAnimations = toPairs(value).map((button) => false);
    }
    this.buttons = toPairs(value);
  }
  get value() {
    return fromPairs(this.buttons);
  }
  isDisabled: boolean;
  onChange = (_: any) => {};
  onTouch = () => {};

  constructor() {}

  ngAfterViewInit(): void {
    const animationDelay = this.buttonAnimations.length > 4 ? 50 : 250;
    from(this.generateRandomOrder(this.buttonAnimations.length))
      .pipe(
        concatMap((value) => of(value).pipe(delay(animationDelay))),
        takeUntil(this.unsubscribeAll)
      )
      .subscribe((index) => {
        this.buttonAnimations[index] = true;
      });
  }

  selectOption(state, j) {
    if (this.mode === 'multiple') {
      this.value = { ...this.value, [state.key]: state.value };
    }
    console.log('state', state, this.value);
    if (this.mode === 'single') {
      this.buttons = this.buttons.map(([key, value]) =>
        state.key === key ? [state.key, state.value] : [key, value]
      );
      this.value = fromPairs(this.buttons);
    }
    this.onTouch();
    this.onChange(this.value);
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  generateRandomOrder(length) {
    const myArray = Array.from(Array(length).keys());
    for (let i = myArray.length - 1; i > 1; i--) {
      const r = Math.floor(Math.random() * i);
      const t = myArray[i];
      myArray[i] = myArray[r];
      myArray[r] = t;
    }
    return myArray;
  }

  writeValue(value: any): void {
    if (value) {
      this.value = value || {};
    } else {
      this.value = {};
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
