import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LayerConfigService } from './layer-config.service';
import { tap } from 'rxjs/operators';

export const CONFIG_URL = 'assets/global-config.json';

@Injectable({ providedIn: 'root' })
export class ConfigResolver implements Resolve<any> {
  constructor(
    private http: HttpClient,
    private layerConfigService: LayerConfigService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return this.http
      .get(CONFIG_URL)
      .pipe(
        tap((globalConfig) =>
          this.layerConfigService.setGlobalConfig(globalConfig)
        )
      );
  }
}
