import { TestBed } from '@angular/core/testing';

import { CustomFormGuard } from './custom-form.guard';

describe('CustomFormGuard', () => {
  let guard: CustomFormGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CustomFormGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
