import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  ActivatedRoute,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { LayerConfigService, LAYER_NAMES } from './layer-config.service';

@Injectable({
  providedIn: 'root',
})
export class CustomFormGuard implements CanActivate {
  constructor(
    private layerConfigService: LayerConfigService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const step = this.layerConfigService.getStepIndexByUrl(state.url);
    let validStep = 0;
    for (let i = 0; i < step; i++) {
      if (this.layerConfigService.stepFormGroups[i].valid) {
        validStep = i + 1;
      } else {
        break;
      }
    }
    if (validStep !== step) {
      this.router.navigate([`app/${LAYER_NAMES[validStep]}`]);
      return false;
    } else {
      return true;
    }
  }
}
