import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, transition, useAnimation } from '@angular/animations';
import { rubberBand } from 'ng-animate';
import { Subject, interval, timer } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { LayerConfigService } from '../layer-config.service';

export const TOTAL_STATES = ['#FFC161', '#FF8E6F', '#E36883', '#A85490'];

@Component({
  selector: 'app-custom-step',
  templateUrl: './custom-step.component.html',
  styleUrls: ['./custom-step.component.scss'],
  animations: [
    trigger('bounce', [transition('true => false', useAnimation(rubberBand))]),
  ],
})
export class CustomStepComponent implements OnInit, OnDestroy {
  states;
  bounce = true;
  hiddeArrow = true;
  currentIndex;
  activeIndex;
  unsubscribeAll = new Subject();
  stepConfig;
  minHeight;
  resize$ = new Subject<void>();

  @HostListener('window:resize', ['$event.target'])
  onResize(target) {
    this.resize$.next(target.innerWidth);
  }

  constructor(
    public route: ActivatedRoute,
    public layerConfigService: LayerConfigService
  ) {
    this.minHeight = `${window.innerHeight - 128}px`;

    this.resize$
      .pipe(debounceTime(300), takeUntil(this.unsubscribeAll))
      .subscribe(
        (innerWidth) => (this.minHeight = `${window.innerHeight - 128}px`)
      );
    const path = this.route.snapshot.routeConfig.path;
    this.activeIndex = this.layerConfigService.getStepIndexByPath(path);
    this.currentIndex = this.layerConfigService.getStepIndexByPath(path);
    this.stepConfig = this.layerConfigService.getStepConfig(this.activeIndex);
    this.layerConfigService.stepFormGroups[this.activeIndex].statusChanges
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((status) => this.setupStepStatus());
    this.layerConfigService.stepFormGroups[this.activeIndex].valueChanges
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((value) => {
        this.layerConfigService.setLocalStorageValue(this.activeIndex, value);
      });

    this.layerConfigService.stepFormGroups[this.activeIndex].setValue(
      this.layerConfigService.getLocalStorageValue(this.activeIndex, path)
    );
  }

  setupStepStatus() {
    this.states = TOTAL_STATES.slice(
      0,
      this.layerConfigService.getLastAvailableStep()
    );
    this.hiddeArrow = this.layerConfigService.getStepByIndex(
      this.activeIndex
    ).invalid;
  }

  ngOnInit(): void {
    this.setupStepStatus();
    interval(800)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe({
        next: () => (this.bounce = !this.bounce),
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  indexChanged(index) {
    timer(250)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((_) => {
        this.layerConfigService.goToStepIndex(index);
      });
  }
}
