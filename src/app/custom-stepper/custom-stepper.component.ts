import {
  Component,
  OnInit,
  ChangeDetectorRef,
  OnDestroy,
  AfterViewInit,
} from '@angular/core';
import { Directionality } from '@angular/cdk/bidi';
import { CdkStepper } from '@angular/cdk/stepper';
import { LayerConfigService } from '../layer-config.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-custom-stepper',
  templateUrl: './custom-stepper.component.html',
  styleUrls: ['./custom-stepper.component.scss'],
  providers: [{ provide: CdkStepper, useExisting: CustomStepperComponent }],
})
export class CustomStepperComponent extends CdkStepper implements OnDestroy {
  unsubscribeAll = new Subject();
  constructor(
    dir: Directionality,
    changeDetectorRef: ChangeDetectorRef,
    private layerConfigService: LayerConfigService,
    private router: Router
  ) {
    super(dir, changeDetectorRef);
    this.layerConfigService.stepIndexSubject.subscribe((index: number) => {
      this.selectedIndex = index;
    });

    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        takeUntil(this.unsubscribeAll)
      )
      .subscribe((event: any) => {
        this.selectedIndex = this.layerConfigService.getStepIndexByUrl(
          event.url
        );
      });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  onClick(index: number): void {
    this.selectedIndex = index;
  }
}
