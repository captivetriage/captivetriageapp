import { TestBed } from '@angular/core/testing';

import { LayerConfigService } from './layer-config.service';

describe('LayerConfigService', () => {
  let service: LayerConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LayerConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
