import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import {
  ValidatorFn,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';

export const LAYER_NAMES = ['language', 'condition', 'symptoms', 'information'];

export function atLeastOneValidator(controlName): ValidatorFn {
  return function validate(formControl: FormControl) {
    if (Object.values(formControl.value).includes(true)) {
      return null;
    }
    return { requireCheck: true };
  };
}

@Injectable({
  providedIn: 'root',
})
export class LayerConfigService {
  private globalConfig;
  stepFormGroups: FormGroup[];
  stepIndexSubject = new Subject();
  constructor(private formBuilder: FormBuilder) {
    this.stepFormGroups = LAYER_NAMES.map((layer, index) =>
      this.formBuilder.group({
        [`${layer}`]: [
          this.getLocalStorageValue(index, layer)[`${layer}`],
          atLeastOneValidator(layer),
        ],
      })
    );
  }

  getGlobalConfig() {
    return this.globalConfig;
  }

  setGlobalConfig(globalConfig) {
    this.globalConfig = globalConfig;
  }

  getStepIndexByPath(path) {
    return LAYER_NAMES.indexOf(path);
  }

  getLayerNameByIndex(index) {
    return LAYER_NAMES[index];
  }

  getStepIndexByUrl(url) {
    return (
      (url.split('/').length === 3 &&
        this.getStepIndexByPath(url.split('/')[2])) ||
      0
    );
  }

  getStepConfig(stepIndex) {
    return this.globalConfig.steps[stepIndex];
  }

  getStepByIndex(index) {
    return this.stepFormGroups[index];
  }

  getLastAvailableStep() {
    let lastStep = 0;
    for (let i = 0; i < this.stepFormGroups.length; i++) {
      this.stepFormGroups[i].markAllAsTouched();
      if (this.stepFormGroups[i].valid) {
        lastStep = i + 1;
      } else {
        break;
      }
    }
    return lastStep + 1;
  }

  goToStepIndex(index) {
    this.stepIndexSubject.next(index);
  }

  getLocalStorageValue(index, path) {
    const rawValue = localStorage.getItem(`${LAYER_NAMES[index]}`);
    return { [`${path}`]: (rawValue && JSON.parse(rawValue)) || {} };
  }

  setLocalStorageValue(index, value) {
    localStorage.setItem(
      `${LAYER_NAMES[index]}`,
      JSON.stringify(value[LAYER_NAMES[index]])
    );
  }
}
