import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayerWrapperComponent } from './layer-wrapper.component';

describe('LayerWrapperComponent', () => {
  let component: LayerWrapperComponent;
  let fixture: ComponentFixture<LayerWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayerWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayerWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
