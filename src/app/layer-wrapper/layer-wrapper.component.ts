import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LayerConfigService } from '../layer-config.service';

@Component({
  selector: 'app-layer-wrapper',
  templateUrl: './layer-wrapper.component.html',
  styleUrls: ['./layer-wrapper.component.scss'],
})
export class LayerWrapperComponent implements OnInit {
  public steps: string[];
  public selectedStep = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public layerConfigService: LayerConfigService
  ) {}

  ngOnInit() {
    this.steps = this.route.snapshot.routeConfig.children.map(
      (child) => child.path
    );
    this.selectedStep = this.layerConfigService.getStepIndexByUrl(
      this.router.url
    );
    const step = this.steps[
      this.layerConfigService.getStepIndexByUrl(this.router.url)
    ];
    this.router.navigate([step], { relativeTo: this.route });
  }

  selectionChanged(event: any) {
    this.selectedStep = event.selectedIndex;
    this.router.navigate([this.steps[this.selectedStep]], {
      relativeTo: this.route,
    });
  }
}
