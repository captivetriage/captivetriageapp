import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-number-picker-dialog',
  templateUrl: './number-picker-dialog.component.html',
  styleUrls: ['./number-picker-dialog.component.scss'],
})
export class NumberPickerDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<NumberPickerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  pickNumber(event): void {
    this.dialogRef.close(event);
  }

  ngOnInit(): void {}
}
