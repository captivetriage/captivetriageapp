import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-result-message',
  templateUrl: './result-message.component.html',
  styleUrls: ['./result-message.component.scss'],
})
export class ResultMessageComponent implements OnInit {
  @Input() loading = true;

  constructor() {}

  ngOnInit(): void {}
}
