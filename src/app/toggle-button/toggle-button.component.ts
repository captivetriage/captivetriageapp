import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {
  trigger,
  transition,
  state,
  style,
  animate,
} from '@angular/animations';
import { NumberPickerDialogComponent } from '../number-picker-dialog/number-picker-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-toggle-button',
  templateUrl: './toggle-button.component.html',
  styleUrls: ['./toggle-button.component.scss'],
  animations: [
    trigger('fabToggler', [
      state(
        'inactive',
        style({
          background: 'rgba(255, 255, 255, 0.5)',
          color: 'black',
          'box-shadow':
            '0px 0px 10px -1px rgba(0, 0, 0, 0.2), 0px 10px 10px 0px rgba(0, 0, 0, 0.14), 0px 18px 18px 0px rgba(0, 0, 0, 0.12)',
        })
      ),
      state(
        'active',
        style({
          background: 'white',
          color: 'black',
        })
      ),
      transition('* <=> *', animate('100ms ease-in-out')),
    ]),
  ],
})
export class ToggleButtonComponent implements OnInit {
  constructor(public dialog: MatDialog) {}
  fabTogglerState = 'inactive';
  @Output() toggle = new EventEmitter<any>();
  @Input() state = [null, null];
  @Input() config = null;

  ngOnInit(): void {
    this.fabTogglerState = !this.state[1] ? 'inactive' : 'active';
  }

  onToggleFab() {
    if (
      this.config &&
      this.config.value.number_picker &&
      this.fabTogglerState === 'inactive'
    ) {
      const dialogRef = this.dialog.open(NumberPickerDialogComponent, {
        width: '350px',
        restoreFocus: false,
        data: {},
      });

      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.toggleButton(result);
        }
      });
    } else {
      this.toggleButton();
    }
  }

  toggleButton(result?) {
    this.fabTogglerState =
      this.fabTogglerState === 'active' ? 'inactive' : 'active';
    if (this.fabTogglerState === 'active') {
      if (this.config && this.config.showDialog) {
        this.state = [this.state[0], result];
      } else {
        this.state = [this.state[0], true];
      }
    } else {
      this.state = [this.state[0], null];
    }
    this.toggle.emit({ value: this.state[1], key: this.state[0] });
  }
}
